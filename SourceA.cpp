#pragma once 
#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
    for (auto it = results.begin(); it != results.end(); ++it)
    {
        it->second.clear();
    }
    results.clear();
}
void printTable()
{
    auto iter = results.end();
    iter--;
    int size = results.begin()->second.size();
    //int size = iter->second.size();
    for (int i = -2; i < size; i++)
    {
        for (auto it = results.begin(); it != results.end(); ++it)
        {
            if (i == -2)
            {
                //cout << it->first << " ";
                printf("|%*s|", 13, it->first.c_str());
            }
            else if (i == -1)
                cout << "_______________";
            else
                //	cout << it->second.at(i) << " ";
                printf("|%*s|", 13, it->second.at(i).c_str());
        }
        cout << endl;
    }
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
    int i;

    for (i = 0; i < argc; i++)
    {
        auto it = results.find(azCol[i]);
        if (it != results.end())
        {
            it->second.push_back(argv[i]);
        }
        else
        {
            pair<string, vector<string>> p;
            p.first = azCol[i];
            p.second.push_back(argv[i]);
            results.insert(p);
        }
    }

    return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
    cout << "the Last id is: " << argv[0] << endl;
    return 0;
}

int main()
{
    int rc;
    sqlite3* db;
    char *zErrMsg = 0;
    bool flag = true;
    char* sql;


    
    // connection to the database
    rc = sqlite3_open("FirstPart.db", &db);

    if (rc)
    {
        cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
        sqlite3_close(db);
        system("Pause");
        return(1);
    }

    system("CLS");
    sql="DROP TABLE IF EXISTS people";
    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    sql = "CREATE TABLE people(id integer primary key autoincrement, name text)";

    rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);
    if (rc != SQLITE_OK)
    {
        cout << "SQL error: " << zErrMsg << endl;
        sqlite3_free(zErrMsg);
        system("Pause");
        return 1;
    }

    sql = "insert into people (name) values('roy')";

    if (flag)
    {
        rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

        if (rc != SQLITE_OK)
        {
            cout << "SQL error: " << zErrMsg << endl;
            sqlite3_free(zErrMsg);
            system("Pause");
            return 1;
        }
    }

    sql = "insert into people (name) values('moshe')";
    if (flag)
    {
        rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

        if (rc != SQLITE_OK)
        {
            cout << "SQL error: " << zErrMsg << endl;
            sqlite3_free(zErrMsg);
            system("Pause");
            return 1;
        }
    }

    sql = "insert into people (name) values('kobi')";
    if (flag)
    {
        rc = sqlite3_exec(db, sql, callback, 0, &zErrMsg);

        if (rc != SQLITE_OK)
        {
            cout << "SQL error: " << zErrMsg << endl;
            sqlite3_free(zErrMsg);
            system("Pause");
            return 1;
        }
    }

    sql = "update people set name='mao' where name = 'kobi'";
    if (flag)
    {

        rc = sqlite3_exec(db,sql, callback, 0, &zErrMsg);
        if (rc != SQLITE_OK)
        {
            cout << "SQL error: " << zErrMsg << endl;
            sqlite3_free(zErrMsg);
            system("Pause");
            return 1;
        }
    }
    
    system("Pause");
    system("CLS");

}