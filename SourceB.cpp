#pragma once 
#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
    for (auto it = results.begin(); it != results.end(); ++it)
    {
        it->second.clear();
    }
    results.clear();
}
void printTable()
{
    auto iter = results.end();
    iter--;
    int size = results.begin()->second.size();
    //int size = iter->second.size();
    for (int i = -2; i < size; i++)
    {
        for (auto it = results.begin(); it != results.end(); ++it)
        {
            if (i == -2)
            {
                //cout << it->first << " ";
                printf("|%*s|", 13, it->first.c_str());
            }
            else if (i == -1)
                cout << "_______________";
            else
                //	cout << it->second.at(i) << " ";
                printf("|%*s|", 13, it->second.at(i).c_str());
        }
        cout << endl;
    }
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
    int i;

    for (i = 0; i < argc; i++)
    {
        auto it = results.find(azCol[i]);
        if (it != results.end())
        {
            it->second.push_back(argv[i]);
        }
        else
        {
            pair<string, vector<string>> p;
            p.first = azCol[i];
            p.second.push_back(argv[i]);
            results.insert(p);
        }
    }

    return 0;
}
/*
the function check if there is enough money for someone to buy some car.
input: buyer id, car id, database etc.
output: the answer for the qustions (TRUE or FALSE)
*/
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
    int rc , b_buyerid, p_carid;
    string sql;

    sql= "select available from cars where id=" + to_string(carid) + ";";  ///////// check availablity.
    sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);

    if ((*results.begin()).second[0] == "0")
    {
        clearTable();
        return false;
    }
    clearTable();

    sql = "select balance from accounts where Buyer_id=" + to_string(buyerid) + ";"; /////////// find 'balance' from 'accounts' table.
    sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);

    if (results.size() > 0)
    {
        b_buyerid = atoi((*results.begin()).second[0].c_str());
    }
    sql = "select price from cars where id=" + to_string(carid) + ";"; ////////////////////// find 'price' from 'cars' table.
    sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);

    if (sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg) != SQLITE_OK) //checking the price of the car
    {
        system("cls");
        system("pause");
        std::cout << zErrMsg << std::endl;
        return 0;
    }

    if (results.size() > 0)
    {
        p_carid = atoi((*results.begin()).second[0].c_str());
    }

    if (b_buyerid < p_carid) /// compare the price and balance.
    {
        return false;
    }

    sql = "update cars set available = 0 where id=" + to_string(carid) + ";";
    sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);

    b_buyerid -= p_carid;

    sql = "update accounts set balance =" + to_string(b_buyerid) + "where Buyer_id=" + to_string(buyerid) + ";";
    sqlite3_exec(db, sql.c_str(), callback, 0, &zErrMsg);

    return true;
}


/*
the function transfer money in two accounts.
input: id of the one to tranfer from, id of the one to tranfer from, amount of money to transfer, database etc...
output: TRUE if the tranfer done. false if not.
*/
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
    int rc, b_buyer;
    string sql;

    sql = "select balance from accounts where id=" + to_string(from) + ";"; /////////// find 'balance' from 'accounts' table. 
    sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);
    if (results.size() > 0)
    {
        b_buyer = atoi((*results.begin()).second[0].c_str());
    }

    if (b_buyer < amount)///// checkong 'balance' vs 'amount'
    {
        return false;
    }

    b_buyer -= amount;
    sql = "update accounts set balance=" + to_string(b_buyer) + "where id=" + to_string(from) + ";";  /////////// set 'balance;' in 'accounts table.
    sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);

    sql = "select balance from accounts where id=" + to_string(to) + ";";
    sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);
    
    if (results.size() > 0)
    {
        b_buyer = atoi((*results.begin()).second[0].c_str());
    }

    b_buyer += amount;

    sql = "update accounts set balance=" + to_string(b_buyer) + "where id=" + to_string(to) + ";";///////////////////////////// update balance from accounts.
    sqlite3_exec(db, sql.c_str(), NULL, 0, &zErrMsg);

    return true;

}



int main()
{
    int rc;
    sqlite3* db;
    char *zErrMsg = 0;
    bool flag = true;
    

    // connection to the database
    rc = sqlite3_open("carsDealer.db" , &db);

    if (rc)
    {
        cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
        sqlite3_close(db);
        system("Pause");
        return(1);
    }

    carPurchase(10, 16, db, 0);

    carPurchase(5, 18, db, 0);

    carPurchase(12, 22, db, 0);
    
  

    system("Pause");
    system("CLS");
    return 0;

}